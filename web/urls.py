from django.conf.urls import url
import views

# from django.urls import path


urlpatterns = [
    url(r'^$',views.index),
    url(r'^home/$',views.home),
    url(r'^farmer-register/$',views.farmer_register),
    url(r'^farmer_register1/$',views.farmer_register1),
    url(r'^buyer-register/$',views.buyer_register),
    url(r'^buyer_register1/$',views.buyer_register1),
    url(r'^login/$',views.login),
    url(r'^login1/$',views.login1),
    url(r'^customer/$',views.customer),
    url(r'^admin/',views.admin),
    url(r'^farmer_verify/$',views.farmer_verify),
    url(r'^farmer_verify1/(\d+)/$',views.farmer_verify1),
    url(r'^farmer_verify2/$',views.farmer_verify2),
    url(r'^farmer_remove/$',views.farmer_remove),
    url(r'^farmer_remove1/(\d+)/$',views.farmer_remove1),
    url(r'^addcategory/$',views.addcategory),
    url(r'^addcategory1/$',views.addcategory1),
    url(r'^removecategory/$',views.removecategory),
    url(r'^removecategory1/(\d+)/$',views.removecategory1),
    url(r'^category_update/$',views.category_update),
    url(r'^category_update1/(\d+)/$',views.category_update1),
    url(r'^category_update2/$',views.category_update2),
    url(r'^farmer/$',views.farmer),
    url(r'^crop-details/$',views.crop_details),
    url(r'^crop_details1/$',views.crop_details1),
    url(r'^crop_remove1/$',views.crop_remove1),
    url(r'^crop_remove2/(\d+)/$',views.crop_remove2),
    url(r'^crop_update/$',views.crop_update),
    url(r'^crop_update1/(\d+)/$',views.crop_update1),
    url(r'^crop_update2/$',views.crop_update2),
    url(r'^about/$',views.about),
    url(r'^blog/$',views.blog),
    url(r'^services/$',views.services),
    url(r'^contact/$',views.contact),
    url(r'^addaccount/$',views.addaccount),
    url(r'^addaccount1/$',views.addaccount1),
    url(r'^removeaccount/$',views.removeaccount),
    url(r'^removeaccount1/(\d+)/$',views.removeaccount1),
    url(r'^panchayath/$',views.panchayath),
    url(r'^announcemela/$',views.announcemela),
    url(r'^announcemela1/$',views.announcemela1),
    url(r'^viewmela/$',views.viewmela),
    url(r'^melaregister/(\d+)/$',views.melaregister),
    url(r'^melaregister1/$',views.melaregister1),
    url(r'^viewregmela/$',views.viewregmela),
    url(r'^viewregmela1/(\d+)/$',views.viewregmela1),
    url(r'^logout/$',views.logout),
    url(r'^viewmelas/$',views.viewmelas),
    url(r'^complaints/$',views.complaints),
    url(r'^complaints1/$',views.complaints1),
    url(r'^viewcomplaints/$',views.viewcomplaints),
    url(r'^review/$',views.review),
    url(r'^givereview/(\d+)/$',views.givereview),
    url(r'^givereview1/$',views.givereview1),
    url(r'^viewreview/(\d+)/$',views.viewreview),
    url(r'^viewcategory/$',views.viewcategory),
    url(r'^viewcrop/$',views.viewcrop),
    url(r'^viewcropcus/(\d+)/$',views.viewcropcus),
    url(r'^viewfarmer/$',views.viewfarmer),
    url(r'^viewcategorycus/$',views.viewcategorycus),
    url(r'^order/(\d+)/$',views.order),
    url(r'^vieworder/$',views.vieworder),
    url(r'^vieworder1/(\d+)/$',views.vieworder1),
    url(r'^addorder/$',views.addorder),
    url(r'^orderaccept/(\d+)/$',views.orderaccept),
    url(r'^orderreject/(\d+)/$',views.orderreject),
    url(r'^orderstatus/$',views.orderstatus),
    url(r'^krishibhavan/$',views.krishibhavan),
    url(r'^addseeds/$',views.addseeds),
    url(r'^addseeds1/$',views.addseeds1),
    url(r'^removeseeds/$',views.removeseeds),
    url(r'^removeseeds1/(\d+)/$',views.removeseeds1),
    url(r'^updateseeds/$',views.updateseeds),
    url(r'^updateseeds1/(\d+)/$',views.updateseeds1),
    url(r'^updateseeds2/$',views.updateseeds2),
    url(r'^addsubcidi/$',views.addsubcidi),
    url(r'^addsubcidi1/$',views.addsubcidi1),
    url(r'^removesubcidi/$',views.removesubcidi),
    url(r'^removesubcidi1/(\d+)/$',views.removesubcidi1),
]




