from django.db import models

class tbcategory(models.Model):
    Name=models.CharField(max_length=20)
    Description=models.CharField(max_length=80)
    Photo=models.CharField(max_length=100)

    
    class Meta:
        db_table="tbcategory"


class tbfreg(models.Model):
    F_ID=models.CharField(max_length=20)
    Name=models.CharField(max_length=20)
    Panjayath=models.CharField(max_length=20)
    Address=models.CharField(max_length=80)
    Phone=models.CharField(max_length=100)
    Email=models.CharField(max_length=30)
    Landarea=models.CharField(max_length=20)
    Photo=models.CharField(max_length=100)
    Status=models.CharField(max_length=20)

    
    class Meta:
        db_table="tbfreg"


class tbcrop(models.Model):
    Category=models.CharField(max_length=30)
    name=models.CharField(max_length=30)
    price=models.CharField(max_length=30)
    Landarea=models.CharField(max_length=20)
    Harvesttime=models.CharField(max_length=20)
    Photo=models.CharField(max_length=100)
    Expectedtime=models.CharField(max_length=20)
    Status=models.CharField(max_length=20)
    F_ID=models.CharField(max_length=100)

    
    class Meta:
        db_table="tbcrop"


class tblogin(models.Model):
    Username=models.CharField(max_length=30)
    Password=models.CharField(max_length=30)
    Category=models.CharField(max_length=30)

    
    class Meta:
        db_table="tblogin"


class tbbuyer(models.Model):
    B_ID=models.CharField(max_length=20)
    Name=models.CharField(max_length=20)
    Address=models.CharField(max_length=60)
    Phone=models.CharField(max_length=100)
    Email=models.CharField(max_length=30)

    
    class Meta:
        db_table="tbbuyer"


class tbaccount(models.Model):
    Panjayath=models.CharField(max_length=30)
    category=models.CharField(max_length=30)
    district=models.CharField(max_length=30)
    Email=models.CharField(max_length=30)
    Password=models.CharField(max_length=30)
  
    class Meta:
        db_table="tbaccount"


class idgen(models.Model):
    F_ID=models.IntegerField()
    B_ID=models.IntegerField()

    
    class Meta:
        db_table="idgen"


class tbmela(models.Model):
    melaname=models.CharField(max_length=30)
    Panjayathname=models.CharField(max_length=40)
    venue=models.CharField(max_length=30)
    startdate=models.CharField(max_length=100)
    time=models.CharField(max_length=100)
    enddate=models.CharField(max_length=100)
    reg_lastdate=models.CharField(max_length=100)
    image=models.CharField(max_length=50)
    status=models.CharField(max_length=100)

    
    class Meta:
        db_table="tbmela"


class melareg(models.Model):
    mela_id=models.IntegerField()
    F_ID=models.CharField(max_length=50)
    crop_details=models.CharField(max_length=40)
    reg_date=models.CharField(max_length=100)
    status=models.CharField(max_length=50)

    
    class Meta:
        db_table="melareg"


class tbcomplaint(models.Model):
    B_ID=models.CharField(max_length=50)
    F_ID=models.CharField(max_length=50)
    complaint=models.CharField(max_length=150)
    comp_date=models.CharField(max_length=100)
    status=models.CharField(max_length=100)

    
    class Meta:
        db_table="tbcomplaint"


class tbreview(models.Model):
    B_ID=models.CharField(max_length=50)
    F_ID=models.CharField(max_length=50)
    crop_id=models.CharField(max_length=50)
    review=models.CharField(max_length=100)
    review_date=models.CharField(max_length=100)

    
    class Meta:
        db_table="tbreview"


class tborder(models.Model):
    BUYER_ID=models.CharField(max_length=10)
    CROP_ID=models.CharField(max_length=10)
    requirement=models.CharField(max_length=100)
    req_qty=models.CharField(max_length=100)
    order_date=models.CharField(max_length=30)
    Status=models.CharField(max_length=10)

    
    class Meta:
        db_table="tborder"

class tbseeds(models.Model):
    Name=models.CharField(max_length=10)
    Unit=models.CharField(max_length=10)

    
    class Meta:
        db_table="tbseeds"

class tbsubcidi(models.Model):
    Category=models.CharField(max_length=30)
    Item_name=models.CharField(max_length=30)
    percentage=models.CharField(max_length=30)

    
    class Meta:
        db_table="tbsubcidi"
# Create your models here.
