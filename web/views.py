from django.shortcuts import render,redirect
from web.models import tbcategory,tbfreg,tbcrop,tblogin,tbbuyer,tbaccount,idgen,tbmela,melareg,tbcomplaint,tbreview,tborder,tbseeds,tbsubcidi
from django.core.files.storage import FileSystemStorage
from django.conf  import settings
import datetime
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError


def index(request):
    return render(request,'web/index.html')
def home(request):
    return render(request,'web/index.html')
def farmer_register(request):
    data=idgen.objects.get(id=1)
    f=data.F_ID
    f=f+1
    f1="FR00"+str(f)
    request.session['F_ID']=f
    return render(request,'web/register.html',{'ff':f1})
def farmer_register1(request):
    data=tbfreg()
    data.F_ID=request.POST.get('formid')
    data.Name=request.POST.get('formname')
    data.Panjayath=request.POST.get('formpanjayath')
    data.Address=request.POST.get('formaddress')
    data.Phone=request.POST.get('formnumber')
    data.Email=request.POST.get('formemail')
    data.Landarea=request.POST.get('formlandarea')
    Photo=request.FILES['photo']
    fs=FileSystemStorage()
    filename=fs.save(Photo.name,Photo)
    uploaded_file_url=fs.url(filename)
    data.Photo=uploaded_file_url
    data.Status="pending"
    data.save()
    f=request.session['F_ID']
    data2=idgen.objects.get(id=1)
    data2.F_ID=f
    data2.save()
    return redirect('/farmer-register')
def buyer_register(request):
    data=idgen.objects.get(id=1)
    f=data.B_ID
    f=f+1
    f1="BR00"+str(f)
    request.session['B_ID']=f
    return render(request,'web/buyer_register.html',{'ff':f1})
def buyer_register1(request):
    data=tbbuyer()
    data.B_ID=request.POST.get('buyerid')
    data.Name=request.POST.get('buyername')
    data.Address=request.POST.get('buyeraddress')
    data.Phone=request.POST.get('buyernumber')
    data.Email=request.POST.get('buyeremail')
    data.save()
    f=request.session['B_ID']
    data2=idgen.objects.get(id=1)
    data2.B_ID=f
    data2.save()
    data1=tblogin()
    data1.Username=data.B_ID
    data1.Password=data.Phone
    data1.Category='customer'
    data1.save()
    return redirect('/buyer-register')
def login(request):
    return render(request,'web/login.html')
def login1(request):
    if request.method=='POST':
        dataa=tblogin.objects.all()
        Username=request.POST.get('loginuser')
        Password=request.POST.get('loginpassword')
        flag=0
        for da in dataa:
            if Username==da.Username and Password==da.Password:
                type=da.Category
                flag=1
                request.session['uid']=Username
                if type=="admin":
                    return redirect('/admin')
                elif type=="customer":
                    return redirect('/customer')
                elif type=="user":
                    return redirect('/user')
                elif type=="panchayath":
                    return redirect('/panchayath')
                elif type=="farmer":
                    return redirect('/farmer')
                elif type=="krishibhavan":
                    return redirect('/krishibhavan')    
                else:
                    return HttpResponse("Invalid acct type")
        if flag==0:
            return HttpResponse("User doesn't exist")
def customer(request):
    return render(request,'web/customer.html')
def admin(request):
    return render(request,'web/admin.html')
def farmer_verify(request):
    data=tbfreg.objects.filter(Status="pending")
    return render(request,'web/verifyfarmer.html',{'td':data})
def farmer_verify1(request,id):
    #data=tbfreg.objects.all()
    data=tbfreg.objects.get(id=id)
    return render(request,'web/verification.html',{'td':data})
def farmer_verify2(request):
    id1=request.POST.get('id1')
    data=tbfreg.objects.get(id=id1)
    data.Status="active"
    data.save()
    data1=tblogin()
    data1.Username=data.F_ID
    data1.Password=data.Phone
    data1.Category='farmer'
    data1.save()
    return render(request,'web/krishibhavan.html')
    #return redirect('/verify')
def farmer_remove(request):
    data=tbfreg.objects.filter(Status='active')
    return render(request,'web/farmer_remove.html',{'td':data})
def farmer_remove1(request,id):
    data=tbfreg.objects.get(id=id)
    fid=data.F_ID
    data.delete()
    data1=tblogin.objects.get(Username=fid)
    data1.delete()
    return redirect('/farmer_remove')
def addcategory(request):
    return render(request,'web/addcategory.html')
def addcategory1(request):
    data=tbcategory()
    data.F_ID=request.POST.get('categoryid')
    data.Name=request.POST.get('categoryname')
    data.Description=request.POST.get('categorydescription')
    Photo=request.FILES['categoryphoto']
    fs=FileSystemStorage()
    filename=fs.save(Photo.name,Photo)
    uploaded_file_url=fs.url(filename)
    data.Photo=uploaded_file_url
    data.save()
    return render(request,'web/addcategory.html')
def removecategory(request):
    data=tbcategory.objects.all()
    return render(request,'web/categoryremove.html',{'tt':data})
def removecategory1(request,id):
    data=tbcategory.objects.get(id=id)
    data.delete()
    return redirect('/removecategory')
    #return redirect('/categoryremove')
def category_update(request):
    data=tbcategory.objects.all()
    return render(request,'web/category_update.html',{'tt':data})
def category_update1(request,id):
    data=tbcategory.objects.get(id=id)
    return render(request,'web/category_update1.html',{'tt':data})
def category_update2(request):
    data=tbcategory.objects.get(id=request.POST.get('id1'))
    data.Name=request.POST.get('categoryname')
    data.Description=request.POST.get('categorydescription')
    data.save()
    return redirect('/category_update')
def farmer(request):
    return render(request,'web/farmer.html')
def crop_details(request):
    data=tbcategory.objects.all()
    return render(request,'web/cropdetails.html',{'d':data})
def crop_details1(request):
    Username=request.session['uid']
    a=tbfreg.objects.get(F_ID=Username)
    data1=tbcrop()
    data1.F_ID=a.F_ID
    data1.Category=request.POST.get('cropcategory')
    data1.Landarea=request.POST.get('croplandarea')
    data1.Harvesttime=request.POST.get('cropharvesttime')
    Photo=request.FILES['photo']
    fs=FileSystemStorage()
    filename=fs.save(Photo.name,Photo)
    uploaded_file_url=fs.url(filename)
    data1.Photo=uploaded_file_url
    data1.Expectedtime=request.POST.get('cropexpectedquantity')
    data1.Status="active"
    data1.name=request.POST.get('name')
    data1.price=request.POST.get('price')
    data1.save()
    return redirect('/crop-details')
def crop_remove1(request):
    uid=request.session['uid']
    data1=tbcrop.objects.filter(F_ID=uid)
    return render(request,'web/cropremove1.html',{'dd':data1})
def crop_remove2(request,id):
    data1=tbcrop.objects.get(id=id)
    data1.delete()
    return redirect('/crop_remove1')
def crop_update(request):
    uid=request.session['uid']
    data1=tbcrop.objects.filter(F_ID=uid)
    return render(request,'web/crop_update.html',{'dd':data1})
def crop_update1(request,id):
    data1=tbcrop.objects.all()
    data1=tbcrop.objects.get(id=id)
    return render(request,'web/crop_update1.html',{'dd':data1})
def crop_update2(request):
    data1=tbcrop.objects.get(id=request.POST.get('id1'))
    data1.Harvesttime=request.POST.get('cropharvesttime')
    data1.Expectedtime=request.POST.get('cropexpectedquantity')
    data1.save()
    return redirect('/crop_update')
def about(request):
    return render(request,'web/about.html')
def blog(request):
    return render(request,'web/blog.html')
def services(request):
    return render(request,'web/services.html')
def contact(request):
    return render(request,'web/contact.html')
def addaccount(request):
    return render(request,'web/addaccount.html')
def addaccount1(request):
    data=tbaccount()
    data.Panjayath=request.POST.get('panchayathname')
    data.category=request.POST.get('category')
    data.district=request.POST.get('district')
    data.Email=request.POST.get('email')
    data.Password=request.POST.get('password')
    data.save()
    d=tblogin()
    d.Username=request.POST.get('email')
    d.Password=request.POST.get('password')
    d.Category=request.POST.get('category')
    d.save()
    return render(request,'web/admin.html')
def removeaccount(request):
    d=tbaccount.objects.all()
    return render(request,'web/removeaccount.html',{'tt':d})
def removeaccount1(request,id):
    d=tbaccount.objects.get(id=id)
    t=tblogin.objects.get(Username=d.Email)
    d.delete()
    t.delete()
    return redirect('/removeaccount')
def panchayath(request):
    return render(request,'web/panchayath.html')
def announcemela(request):
    Username=request.session['uid']
    a=tbaccount.objects.get(Email=Username)
    return render(request,'web/announcemela.html',{'a1':a})
def announcemela1(request):
    t=tbmela()
    t.melaname=request.POST.get('melaname')
    t.Panjayathname=request.POST.get('panjayath')
    t.venue=request.POST.get('venue')
    t.startdate=request.POST.get('startingdate')
    t.time=request.POST.get('time')
    t.enddate=request.POST.get('enddate')
    t.reg_lastdate=request.POST.get('reglastdate')
    image=request.FILES['f1']
    fs=FileSystemStorage()
    Filename=fs.save(image.name,image)
    uploaded_file_url=fs.url(Filename)
    t.image=uploaded_file_url
    t.status="Not Processed"
    t.save()
    return render(request,'web/panchayath.html')
def viewmela(request):
    t=tbmela.objects.all()
    return render(request,'web/viewmela.html',{'tt':t})
def melaregister(request,id):
    t=tbmela.objects.get(id=id)
    Username=request.session['uid']
    a=tbfreg.objects.get(F_ID=Username)
    return render(request,'web/melaregister.html',{'tt':t,'a1':a})
def melaregister1(request):
    m=melareg()
    mname=request.POST.get('melaname')
    m1=tbmela.objects.get(melaname=mname)
    m.mela_id=m1.id
    fname=request.POST.get('farmername')
    f1=tbfreg.objects.get(Name=fname)
    m.F_ID=f1.F_ID
    m.crop_details=request.POST.get('cropdetails')
    now=datetime.datetime.now()
    time1=now.strftime("%Y-%m-%d")
    m.reg_date=time1
    m.status="not processed"
    m.save()
    m1.status="Processed"
    m1.save()
    return render(request,'web/farmer.html')
def viewregmela(request):
    t=melareg.objects.filter(status='not processed')
    return render(request,'web/viewmelareg.html',{'tt':t})
def viewregmela1(request,id):
    t=melareg.objects.get(id=id)
    t.status="processed"
    t.save()
    return render(request,'web/panchayath.html')
def logout(request):
    return render(request,'web/index.html')
def viewmelas(request):
    t=tbmela.objects.all()
    return render(request,'web/viewmelas.html',{'tt':t})
def complaints(request):
    f=tbfreg.objects.all()
    return render(request,'web/complaint.html',{'f1':f})
def complaints1(request):
    Username=request.session['uid']
    a=tbbuyer.objects.get(B_ID=Username)
    c=tbcomplaint()
    c.B_ID=a.B_ID
    c.F_ID=request.POST.get('fid')
    c.complaint=request.POST.get('complaint')
    now=datetime.datetime.now()
    time2=now.strftime("%Y-%m-%d")
    c.comp_date=time2
    c.status="Not Processed"
    c.save()
    return render(request,'web/customer.html')
def viewcomplaints(request):
    data = tbcomplaint.objects.all()
    context = {
        "complaints":data
    }
    return render(request,'web/viewcomplaints.html',context)
def review(request):
    c=tbcrop.objects.all()
    return render(request,'web/review.html',{'h':c})
def givereview(request,id):
    f=tbcrop.objects.get(id=id)
    return render(request,'web/givereview.html',{'f1':f})
def givereview1(request):
    f=tbcrop.objects.get(id=request.POST.get('id1'))
    Username=request.session['uid']
    a=tbbuyer.objects.get(B_ID=Username)
    t=tbreview()
    t.B_ID=a.B_ID
    t.F_ID=f.F_ID
    t.crop_id=f.name
    t.review=request.POST.get('review')
    now=datetime.datetime.now()
    time2=now.strftime("%Y-%m-%d")
    t.review_date=time2
    t.save()
    return render(request,'web/review.html')
def viewreview(request,id):
    v=tbreview.objects.filter(id=id)
    return render(request,'web/viewreview.html',{'v1':v})
def viewcategory(request):
    obj=tbcategory.objects.all()
    return render(request,'web/viewcategory.html',{'hs':obj})  
def viewcrop(request):
    obj=tbcrop.objects.all()
    return render(request,'web/viewcrop.html',{'hs':obj})  
def viewfarmer(request):
    obj=tbfreg.objects.filter(Status='active')
    return render(request,'web/viewfarmer.html',{'hs':obj})  
def viewcategorycus(request):
    obj=tbcategory.objects.all()
    return render(request,'web/viewcategorycus.html',{'hs':obj})  
def viewcropcus(request,id):
    data=tbcategory.objects.get(id=id)
    cat=data.Name
    obj=tbcrop.objects.filter(Category=cat)
    return render(request,'web/viewcropcus.html',{'hs':obj})  
def order(request,id):
    a=id
    return render(request,'web/order.html',{'h':a})
def vieworder(request):
    obj=tborder.objects.filter(Status='processing')
    return render(request,'web/vieworder.html',{'hs':obj})
def krishibhavan(request):
    return render(request,'web/krishibhavan.html') 
def addseeds(request):
    return render(request,'web/addseeds.html')
def addseeds1(request):
    data=tbseeds()
    data.Name=request.POST.get('seedsname')
    data.Unit=request.POST.get('seedsunit')
    data.save()
    return render(request,'web/krishibhavan.html')
def removeseeds(request):
    data = tbseeds.objects.all()
    context = {
        "d" : data
    }
    return render(request, 'web/removeseeds.html',context)
def removeseeds1(request,id):
    data = tbseeds.objects.get(id=id)
    data.delete()

    return redirect('/removeseeds')
def updateseeds(request):
    data=tbseeds.objects.all()
    return render(request,'web/updateseeds.html',{'d':data})
def updateseeds1(request,id):
    data=tbseeds.objects.get(id=id)
    return render(request,'web/updateseeds1.html',{'d':data})
def updateseeds2(request):
    data=tbseeds.objects.get(id=request.POST.get('id1'))
    data.Name=request.POST.get('name')
    data.Unit=request.POST.get('unit')
    data.save()
    return redirect('/updateseeds')
def addsubcidi(request):
    return render(request,'web/addsubcidi.html')
def addsubcidi1(request):
    data=tbsubcidi()
    data.Category=request.POST.get('category')
    data.Item_name=request.POST.get('itemname')
    data.percentage=request.POST.get('percentage')
    data.save()
    return render(request,'web/krishibhavan.html')
def removesubcidi(request):
    data = tbsubcidi.objects.all()
    context = {
        "d" : data
    }
    return render(request, 'web/removesubcidi.html',context)
def removesubcidi1(request,id):
    data = tbsubcidi.objects.get(id=id)
    data.delete()

    return redirect('/removesubcidi')
def addorder(request):
    a=request.session['uid']
    data=tborder()
    data.BUYER_ID=a
    data.CROP_ID=request.POST.get('cropid')
    data.requirement=request.POST.get('requirement')
    data.req_qty=request.POST.get('quantity')
    now=datetime.datetime.now()
    time1=now.strftime("%y-%m-%d")
    data.order_date=time1
    data.Status='processing'
    data.save()
    return render(request,'web/order.html')
def vieworder1(request,id):
        ob=tborder.objects.get(id=id)
        return render(request,'web/vieworder1.html',{'hs':ob}) 
def orderaccept(request,id):
        ob=tborder.objects.get(id=id)
        ob.Status='accepted'
        ob.save()
        return render(request,'web/vieworder.html') 
def orderreject(request,id):
        ob=tborder.objects.get(id=id)
        ob.Status='rejected'
        ob.save()
        return render(request,'web/vieworder.html') 
def orderstatus(request):
        a=request.session['uid']
        obj=tborder.objects.filter(BUYER_ID=a)
        # a=obj.lab_test_result_id
        # x=obj.CROP_ID
        # b=tbcrop.objects.get(id=x)
        # c=b.photo
        # c=b.lab_result
        # d=obj.doctor_id
        # e=tbl_staff.objects.get(id=d)
        # k=e.staff_name
        # f=obj.hospital_id
        # g=tbl_hos_man.objects.get(id=f)
        # l=g.hos_name
        return render(request,'web/orderstatus.html',{'hs':obj})  


    
    



    

    






    

    

    
# Create your views here.
